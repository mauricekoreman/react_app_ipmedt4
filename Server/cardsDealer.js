function dealer(cardArr, users) {
    var allHandsArr = [];
    
    for (var k = 0; k < users.length; k++) {
      var randomHandArr = [];
      for (var i = 0; i < 4; i++) {
        // er wordt hier een random kaart uit de array gekozen om uit te delen
        var randomCard = cardArr[Math.floor(Math.random() * cardArr.length)];
        var cardID = randomCard.card_id;
        randomHandArr.push(randomCard);
  
        // de random kaart wordt uit de cardArr gehaald
        cardArr = cardArr.filter(function(card) {
          return card.card_id != cardID;
        });
      }
      allHandsArr.push(randomHandArr);
    }
      
    return { allHandsArr, cardArr };
  }

function randomCardReturner(cardArr) {
  var filteredCard = cardArr[Math.floor(Math.random() * cardArr.length)];
  var cardID = filteredCard.card_id;

  // de random kaart wordt uit de cardArr gehaald
  filteredCardArr = cardArr.filter(function(card) {
    return card.card_id != cardID;
  });

  // wat is de kaart?

  return { filteredCardArr, filteredCard }
}

module.exports = { dealer, randomCardReturner };