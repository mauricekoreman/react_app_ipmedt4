const shuffleDeck = (cardArr) => {
    var currentIndex = cardArr.length;
    var temporaryValue, randomIndex;
    
    // While there remain elements to shuffle...
    while (0 != currentIndex) {
        // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
    
            // And swap it with the current element.
            temporaryValue = cardArr[currentIndex];
            cardArr[currentIndex] = cardArr[randomIndex];
            cardArr[randomIndex] = temporaryValue;
    }
    
    return cardArr;
}

module.exports = { shuffleDeck };