// get dependencies / libraries
const cors = require('cors');
const socketio = require('socket.io');
const express = require('express');
const axios = require('axios');

// define socket variables
const http = require('http');
const router = require('./router');
const app = express();
const server = http.createServer(app);
const io = socketio(server);

// get User functions from ./users
const { addUser, removeUser, getUser, getUsersInRoom } = require('./users');
const { resetGame, readyCheck, placeACard, plusOfMin, eenOfElf } = require('./logica_of_demarreren');

const users = require('./users');
const cardsDealer = require('./cardsDealer');
const { count } = require('console');

// create an array of users
var clockwise = true;
var usersReadyArr = [];
var usersReadyIDsArr = [];
var usersInRoomArr; // moet vervangen worden voor een check hoeveel users er nu in de room zitten..



app.use(cors());
app.use(router);

// hij haalt hier via de API de kaarten op en plempt ze in 'cardArr'
var UnfilteredCardArr = []; // de array die 'cardArr[]' gebruikt om zichzelf te vullen met mooiere data
var cardArr = []; // de uiteindelijke array met alle kaarten

var dealerValues;
var overgeblevenDeck;

var spelerAanDeBeurt;


var socketid;

axios.get('http://localhost:8000/api/cards')
.then(res => {
    UnfilteredCardArr.push(res.data);
    console.log('ik ben aan het ophalen');

    for (var i = 0 ; i < 52; i++) {
      cardArr.push(UnfilteredCardArr[0].data[i]);
    }

    // console.log(cardArr);
})
.catch(error => {
    console.log(error);
});

io.on('connect', (socket) => {
  
  // console.log(socket.id);

  // listens to when a new user joins a room
  socket.on('join', ({ name, room }, callback) => {
    const { error, user } = addUser({ id: socket.id, name, room });

    if(error) return callback(error);

    socket.join(user.room);

    io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });
    usersInRoomArr = getUsersInRoom(user.room).length;

    callback();
  });

  // when a user disconnects, it gets removed from the users array
  socket.on('disconnect', () => {
    const user = removeUser(socket.id);
    
    if(user) {;
      io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room)});
      usersInRoomArr = getUsersInRoom(user.room).length;

    }
  });

  socket.on('resetGame', () =>{
    resetGame(io);  
  });

  socket.on('readyCheck', (username)  => {
    socketid = socket.id
    readyCheck(io, username, socketid, usersReadyIDsArr, usersInRoomArr, usersReadyArr, dealerValues, cardsDealer, cardArr);
    });


    socket.on('placeACard', function(data) {
      placeACard(data ,cardsDealer, io, usersReadyIDsArr, clockwise);
    });

    socket.on('plusOfMin', function(data){
      plusOfMin(data, io);
    })
    socket.on('eenOfElf', function(data){
      eenOfElf(data, io);
    })
});

server.listen(process.env.PORT || 5000, () => console.log(`Server has started.`));
