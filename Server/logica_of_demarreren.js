var beurtCounter = 0;

function resetGame(io){
  console.log('reset nu')
  
  io.emit('waardeKaartenReset');
  io.emit('readyBtnVisible');
  io.emit('handNotVisible');

  return usersReadyIDsArr = [], usersReadyArr = [], dealerValues = [];
}


function readyCheck(io, username, socketid, usersReadyIDsArr, usersInRoomArr, usersReadyArr, dealerValues, cardsDealer, cardArr){

  var userExists = usersReadyArr.some(user => user.name === username.name);

  // the user has clicked on ready
  if (!userExists) {
    usersReadyArr.push(username);
    usersReadyIDsArr.push(socketid);
    
  } else {
    console.log('User ' + username + ' already exists. Nothing will happen')
    
  }


  // check of iedere user ready heeft geklikt
  if (usersReadyArr.length === usersInRoomArr) {
    spelerAanDeBeurt = usersReadyIDsArr[0];

    //hand en aflegstapel laten zien
    io.emit('handVisible')
    io.emit('cardVisible')
    
    // deel kaarten uit aan spelers
    dealerValues = (cardsDealer.dealer(cardArr, usersReadyArr));
    overgeblevenDeck = dealerValues.cardArr;

    // toon de juiste kaarten aan de juiste speler
    for (let i = 0; i < usersReadyArr.length; i++) {
      io.to(usersReadyIDsArr[i]).emit('handFiller', { id: usersReadyIDsArr[i], handArr: dealerValues.allHandsArr[i] });
    }
    }
    
  };

  //Als een speler een kaart heeft gespeeld wordt er hier de waarde van de kaart naar alle spelers gestuurd
  //De speler krijgt een nieuwe kaart gedeeld
  function placeACard(data ,cardsDealer, io, usersReadyIDsArr, clockwise) {
    var id = data.id;
    var position = data.position;
    var cardValue = data.cardValue;
    var cardImage = data.cardImage;


    console.log(id);
    console.log(spelerAanDeBeurt);

      //Alleen als de speler aan de beurt is kan deze een kaart opgooien
     if (id === spelerAanDeBeurt) {

      var willekeurigeKaartObj;
      willekeurigeKaartObj = cardsDealer.randomCardReturner(overgeblevenDeck);
      overgeblevenDeck = willekeurigeKaartObj.filteredCardArr;

      //Stuurt waarde van de kaart door naar de spelers
      io.emit('waardeDoorgeven', {cardValue: cardValue, cardImage: cardImage})
      io.to(usersReadyIDsArr[beurtCounter]).emit('medlingDoorgeven', {cardValue: cardValue})

      //Stuur een nieuwe kaart terug als er een kaart gespeeld is
      switch (position) {
        case 1:
          io.to(usersReadyIDsArr[beurtCounter]).emit('nieuweKaart1', willekeurigeKaartObj.filteredCard);
          break;
        case 2:
          io.to(usersReadyIDsArr[beurtCounter]).emit('nieuweKaart2', willekeurigeKaartObj.filteredCard);
          break;
        case 3:
          io.to(usersReadyIDsArr[beurtCounter]).emit('nieuweKaart3', willekeurigeKaartObj.filteredCard);
          break;
        case 4:
          io.to(usersReadyIDsArr[beurtCounter]).emit('nieuweKaart4', willekeurigeKaartObj.filteredCard);
          break;
        default:
          console.log('er iets mis gegaan met de positie');
          break;
      }

      //Als er een koning wordt opgegooid wordt hier de spelvolgorde veranderd
      if (clockwise === true){
        beurtCounter++;
        if(cardValue === 13){
          clockwise = false
          console.log('goede kant')
        }
      }
      else{
        beurtCounter--;
        if(cardValue === 13){
          clockwise = true
          console.log('foute kant')
        }
      }

      spelerAanDeBeurt = usersReadyIDsArr[beurtCounter];
      console.log('beurcounter++: ' + beurtCounter);
      console.log(usersReadyIDsArr.length);

    }

    //De eerste speler is hier weer aan de buurt als iedereen geweest is
    if (beurtCounter >= usersReadyIDsArr.length) {
      beurtCounter = 0;
      spelerAanDeBeurt = usersReadyIDsArr[beurtCounter];
    }

    if (beurtCounter < 0){
      beurtCounter = usersReadyIDsArr.length - 1;
      spelerAanDeBeurt = usersReadyIDsArr[beurtCounter];
    }
  };

  //Stuurt de plus of min input na een 10 terug naar de spelers
  function plusOfMin(data, io){
    var minOfplus = data.plusOfMin
    io.emit('plusOfMinTerug', {plusOfMin: minOfplus})
    console.log(minOfplus)
  }
  //Zelfde maar dan met aas
  function eenOfElf(data, io){
    var elfOfEen = data.eenOfElf
    io.emit('eenOfElfTerug', {eenOfElf: elfOfEen})
    console.log(elfOfEen)
  }


module.exports = { resetGame, readyCheck, placeACard, plusOfMin, eenOfElf};