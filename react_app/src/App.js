import React  from 'react';

import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";

import Demarreren from "./pages/demarreren/Demarreren";
import Pauze from "./pages/pauze/Pauze";
import Aanmelden from "./pages/aanmelden/Aanmelden";
import Join from './components/Join/Join';


import './App.css';

const App = () => {
  return (
    <Router>
        <Switch>
          <Route path="/" exact component={Join} />
          <Route path="/demarreren" component={Demarreren} />
          <Route path="/Just Chatting" component={Pauze} />
          <Route path="/join" exact component={Aanmelden} />
        </Switch>
    </Router>
  );
};

export default App;
