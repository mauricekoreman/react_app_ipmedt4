import React, { useState, useEffect } from "react";

// import socket.io meuk
import { useLocation } from "react-router-dom";
import {socket} from '../../Socket'

// import styling
import './aflegstapel.css'

import table from '../../img/poker_table-png.png';
import cardBack from '../../img/red_back.png';

var waardeKaarten = 0;

//Geeft een melding als de waarde van de aflegstapel deelbaar is door 10 of 11,
//Bij deelbaar door 10 mag je slokken uitdelen, bij 11 moet je zelf drinken.
function slokkenDrinken(){
    if (waardeKaarten % 10 === 0 && waardeKaarten !== 0){
        var aantalSlokken = (waardeKaarten / 10);
        if(aantalSlokken === 1){
            window.alert(aantalSlokken + " slokk uitdelen!");
        }
        else{
            window.alert(aantalSlokken + " slokken uitdelen!");
        }
    }
    if (waardeKaarten % 11 === 0 && waardeKaarten !== 0){
        var aantalSlokken = (waardeKaarten / 11);
        if(aantalSlokken === 1){
            window.alert("Dat wordt " + aantalSlokken + " slok drinken!");
        }
        else{
            window.alert("Dat wordt " + aantalSlokken + " slokken drinken!");
        }
    }
}

//Stuurt een reset emit naar de socket server en laat aan de speler zien dat het spel klaar is
function resetGame(){
    if(waardeKaarten >= 100){
        window.alert("Zuipen!!");
        socket.emit('resetGame')
    }
    if(waardeKaarten === 110){
        window.alert("Zuipen en iemand meenemen!");
    }
}

const Aflegstapel = () => {
    const ENDPOINT = 'localhost:5000';
    const location = useLocation();
    const URL = 'http://localhost:8000';
    const [cardValue, setCardValue] = useState(0);
    const [cardImage, setCardImage] = useState(cardBack);
    const [cardVisible, setCardVisible] = useState(false);
    
    useEffect(() => {
        //Zet de waarde van de aflegstapel op 0 zodat er opnieuw gespeeld kan worden
        socket.on('waardeKaartenReset', () => {
            waardeKaarten = 0
            setCardValue(0)
            window.alert("Spel is klaar! Kaarten worden opnieuw geschud!");
        });

        //Krijgt de waarde vam de opgelegde kaart door en laat deze aan de spelers zien
        socket.on('waardeDoorgeven', function(data){
            if(waardeKaarten < 100){
                setCardImage(data.cardImage)
                if (data.cardValue > 1 && data.cardValue < 10){
                    waardeKaarten = waardeKaarten + data.cardValue
                    setCardValue(waardeKaarten)
                    slokkenDrinken();
                }
                if (data.cardValue === 11){
                    waardeKaarten = 96
                    setCardValue(96)
                }  
            }
            if(waardeKaarten >= 100){
                resetGame(); 
                setCardVisible(cardVisible)
            }
            
        })

        //Als de speler een 10 heeft opgegooid mag die kiezen tussen +10 en -10
        //Hier wordt die 10 opgeteld of afgetrokken
        socket.on('plusOfMinTerug', function(data){
            if(data.plusOfMin === '+'){
                waardeKaarten = waardeKaarten + 10
                setCardValue(waardeKaarten)
                slokkenDrinken();
            }
            else if(data.minOfPlus === "-" ){
                waardeKaarten = waardeKaarten - 10
                setCardValue(waardeKaarten)
                slokkenDrinken();
            }
            if(waardeKaarten >= 100){
                resetGame(); 
                setCardVisible(cardVisible)
            }
        })

        //Zelfde als hierboven, alleen met een Aas en +1 en +11
        socket.on('eenOfElfTerug', function(data){
            if(data.eenOfElf === "1"){
                waardeKaarten = waardeKaarten + 1
                setCardValue(waardeKaarten)
                slokkenDrinken();

                }
            else if(data.eenOfElf === "11" ){
                waardeKaarten = waardeKaarten + 11
                setCardValue(waardeKaarten)
                slokkenDrinken();
            }
            if(waardeKaarten >= 100){
                resetGame(); 
                setCardVisible(cardVisible)
            }
        })

        //Als 10 of Aas zal er aan de speler een keuze worden voorgelegd. Dit gebeurt hier
        socket.on('medlingDoorgeven', function(data){
            if(data.cardValue === 10){
                var plusOfMin = window.prompt("+ of -?");
                socket.emit('plusOfMin', {plusOfMin: plusOfMin})
                console.log(plusOfMin)
            }
            if(data.cardValue === 1){
                var eenOfElf = window.prompt("1 of 11?");
                socket.emit('eenOfElf', {eenOfElf: eenOfElf})
                console.log(eenOfElf)
            }
        })

        //Zorgt ervoor dat de kaarten in het midden alleen zichbaar zijn als er een spel actief is.
        socket.on('cardVisible', () =>{
            setCardVisible(!cardVisible)
        })
        
    }, [ENDPOINT, location.search]);

    console.log(cardValue)

    return(
        <section className='speelveld'>
            <img className='speelveld__table' src={table} alt='green felt table top view' />
            <span className='card-value'>{cardValue}</span>
            <div className='speelveld__aflegstapel'>
                <img className={cardVisible ? 'cardsFront' : 'cardsFront--visible'} src={cardImage} alt='card that was played'/>
            </div>
            <div className='speelveld__deck'>
                <img className={cardVisible ? 'cardsBack' : 'cardsBack--visible'} src={cardBack} alt='deck you get cards from' />
            </div>
        </section>
    );
}

export default Aflegstapel;
