import React from 'react';
import './Game.css';
import SubLobby from "../SubLobby/SubLobby"
import SpelerHand from "../SpelerHand/SpelerHand";
import ReadyBtn from "../ReadyBtn/ReadyBtn";
import AflegStapel from '../AflegStapel/Aflegstapel';

const Game = () => {
    return (
        <section className='game'>
            <div className='background'></div>
            <SubLobby />
            <AflegStapel />
            <ReadyBtn />
            <SpelerHand />
        </section>

    );
}

export default Game;
