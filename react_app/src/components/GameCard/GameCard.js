import React, { useState, useEffect } from "react";
import "./GameCard.css";
import queryString from 'query-string';
import { useLocation, NavLink } from "react-router-dom";

const GameCard = (props) => {
  const [name, setName] = useState('');
  const [room, setRoom] = useState('');
  const location = useLocation();
  const ENDPOINT = 'localhost:5000';
  let roomnaam;

  useEffect(() => {
    const { name, room } = queryString.parse(location.search);
    setName(name);
    setRoom(room);
    roomnaam = room;
    
    if (roomnaam.includes("Just Chatting")){
      setRoom(roomnaam.replace("Just Chatting",""))
    }
    if (roomnaam.includes("Demarreren")){
      setRoom(roomnaam.replace("Demarreren",""))
    }
    
  }, [ENDPOINT, location.search]);

  return (
      <NavLink activeClassName='active' className="card" onClick={e => (!name || !room) ? e.preventDefault() : null} to={`/${props.gametitle}?name=${name}&room=${room}${props.gametitle}`}>
        <p className="card__name heading-tertiary"> {props.gametitle || "Pauze"} </p>
      </NavLink>
  );
}

export default GameCard;



