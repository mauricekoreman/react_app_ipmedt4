import React from 'react';
import GameCard from "../GameCard/GameCard";
import ModalVisual from '../../components/RuleModal/ModalVisual';

import './GameDashboard.css';
import '../GameCard/GameCard.css';


const GameDashboard = () => {
  function cardClicked(url) {
    this.props.cardClicked(url);
  }

  return (
    <nav className="sidebar">
      <GameCard
        gametitle = "Just Chatting"
        url = "pauze1"
        cardClicked = {cardClicked}
      />
      <GameCard
        gametitle = "Demarreren"
        url = "demarreren"
        cardClicked = {cardClicked}
      />
      <ModalVisual />
     
    </nav>
  );
}

export default GameDashboard;
