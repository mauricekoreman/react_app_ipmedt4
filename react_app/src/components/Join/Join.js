import React, { useState } from 'react';
import { Link } from "react-router-dom";

import './Join.css';
import QPLogo from '../../img/CLASSIC_LOGO@3x.png';

export default function SignIn() {
  const [name, setName] = useState('');
  const [room, setRoom] = useState('');

  return (
    <section className="join__section">
      <div className='join__bg'></div>
      <figure className='QPLogo__figure'>
        <img className='QPLogo__figure__img' src={QPLogo} alt='Logo Quarantine Productions' />
      </figure>
        
      <form onSubmit="" className="join__form">
        <h2 className="join__form__header">Start een borrel</h2>
        <div className='join__form__input__container'>
          <input placeholder="Naam" className="join__form__input join__form__input--naam" type="text" onChange={(event) => setName(event.target.value)}/>
          <input placeholder="Room" className="join__form__input join__form__input--room" type="text" onChange={(event) => setRoom(event.target.value)}/>
          
          <Link onClick={e => (!name || !room) ? e.preventDefault() : null} to={`/Just Chatting?name=${name}&room=${room}Just Chatting`}>
          <button className='join__form__button' type="submit">Start</button>
        </Link>
        </div>
      </form>
    </section>
  );
}
