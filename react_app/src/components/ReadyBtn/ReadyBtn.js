import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import queryString from 'query-string';
import {socket} from '../../Socket'

import './ReadyBtn.css';



const ReadyBtn = () => {
    const [name, setName] = useState('');
    const location = useLocation();
    const ENDPOINT = 'localhost:5000';
    const [btnVisible, setBtnVisible] = useState(true);
    

    useEffect(() => {
        const { name } = queryString.parse(location.search);
        setName(name);
        socket.on('readyBtnVisible', () => {
            setBtnVisible(btnVisible);
        }) 

    }, [ENDPOINT, location.search]);

    function readyFunction() {
        setBtnVisible(!btnVisible);
        socket.emit('readyCheck', { name }); 
    }

    return (
        <button className={btnVisible ? 'ready__btn': 'ready__btn--notvisible'} onClick={readyFunction}>Ready</button>
    );
}

export default ReadyBtn;