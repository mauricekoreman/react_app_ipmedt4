import React, {forwardRef, useImperativeHandle} from "react";
import './RuleModal.css';
import RuleModal from '../RuleModal/RuleModal';


function App() {
  const modalRef = React.useRef();

  const openModal = () => {
    modalRef.current.openModal()
  };

  return (
    <section className="modal">
      <button className = 'modal__button modal__button--open' onClick={openModal}>Spelregels</button>
      <RuleModal ref={modalRef}>
        <h3 className='modal__heading modal__heading--tertiary'>Spelregels:</h3>

        <section className='modal__content'>
          <article className = 'modal__content__linkerkolom'>
          <h4 className='modal__heading modal__heading--quaternary'>Demarreren</h4>
            <p className='modal__text'>
              Bij demarreren beginnen we bij 0. De eerste speler legt een kaart op,
              en vanaf daar tellen we omhoog. Heeft de eerste speler bijvoorbeeld een 3 opgelegd,
              dan is het nu:
            </p>

            <p className='modal__text'>
              0 + 3 = 3
            </p>

            <p className='modal__text'>
              Wanneer de volgende speler een 8 oplegt wordt het 11, enzovoorts.
            </p>


            <p className='modal__text'>
              De speler die OVER de 100 komt heeft verloren
              en moet een adt trekken of een shot nemen.
            </p>

            <p className='modal__text'>
              Het is mogelijk om jezelf te "beschermen" (of een ander te naaien!)
              als je dicht bij de 100 komt door pestkaarten op te gooien. Wat ze
              precies doen staat hiernaast beschreven.
            </p>

            <p className='modal__text'>
              Als de speler op 110 komt (dit kan met een aas),
              dan mag hij/zij een andere speler mee nemen en moeten zij samen
              een adt of shot nemen.
            </p>
          </article>

          <article className = 'modal__content__rechterkolom'>
            <h4 className='modal__heading modal__heading--quaternary'>Pestkaarten</h4>
            <p className='modal__text'>
              10 = +10 of -10
            </p>
            <p className='modal__text'>
              Boer = 96
            </p>
            <p className='modal__text'>
              Vrouw = niks waard
            </p>
            <p className='modal__text'>
              Koning = doorzichtig && draait spel
            </p>
            <p className='modal__text'>
              Aas =  1 of 11
            </p>
            <p className='modal__text'>
              Tientallen = (tiental / 10) slokken uitdelen
            </p>
            <p className='modal__text'>
              Elftallen = (elftal / 11) slokken incasseren
            </p>
          </article>
        </section>

        <button className = 'modal__button modal__button--close' onClick={() => modalRef.current.close()}>
          sluiten
        </button>
      </RuleModal>
    </section>
  );

}

export default App
