import React, {forwardRef, useImperativeHandle} from "react";
import './RuleModal.css';
import ReactDOM from "react-dom";



const Modal = forwardRef((props,ref) => {
  const [display, setDisplay] = React.useState(false);

  useImperativeHandle(ref, () => {
    return {
      openModal: () => open(),
      close: () => close()
    }
  });

  const open = () => {
    setDisplay(true)
  };

  const close = () => {
    setDisplay(false);
  };


  if (display) {
    return ReactDOM.createPortal(
      <div className={"modal-wrapper"}>
        <div onClick={close} className={"modal-backdrop"} />
        <section className={"modal-box"}>
          {props.children}
        </section>
      </div>, document.getElementById("root"))
  }

  return null;

});

export default Modal
