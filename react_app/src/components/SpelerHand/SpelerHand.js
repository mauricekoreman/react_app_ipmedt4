import React, { useState, useEffect } from "react";

// import socket.io meuk
import { useLocation } from "react-router-dom";
import {socket} from '../../Socket'

// import styling
import './SpelerHand.css'

const SpelerHand = () => {
  const ENDPOINT = 'localhost:5000';
  const location = useLocation();
  const [cardImg, setCardImg] = useState('');
  const URL = 'http://localhost:8000';


  const [kaart1, setKaart1] = useState('');
  const [kaart2, setKaart2] = useState('');
  const [kaart3, setKaart3] = useState('');
  const [kaart4, setKaart4] = useState('');
 
  const [kaartValue1, setKaartValue1] = useState('');
  const [kaartValue2, setKaartValue2] = useState('');
  const [kaartValue3, setKaartValue3] = useState('');
  const [kaartValue4, setKaartValue4] = useState('');

  const [idState, setIdState] = useState('');
 
  const [handVisible, setHandVisible] = useState(false);

  useEffect(() => {
    //Laat de eerste hand zien aan spelers als het spel begonnen is
    socket.on('handFiller', function(data) {
      var handArr = data.handArr;
      setIdState(data.id);
      
      setKaart1(URL + handArr[0].card_image);
      setKaartValue1(handArr[0].card_value);
 
      setKaart2(URL + handArr[1].card_image);
      setKaartValue2(handArr[1].card_value);
 
      setKaart3(URL + handArr[2].card_image);
      setKaartValue3(handArr[2].card_value);
 
      setKaart4(URL + handArr[3].card_image);
      setKaartValue4(handArr[3].card_value);
    }); 

    //Laat een nieuwe kaart zien als een speler een kaart gespeeld heeft
    socket.on('nieuweKaart1', (newCard) => {
      setKaart1(URL + newCard.card_image);
      setKaartValue1(newCard.card_value);
    });

    socket.on('nieuweKaart2', (newCard) => {
      setKaart2(URL + newCard.card_image);
      setKaartValue2(newCard.card_value);
    });

    socket.on('nieuweKaart3', (newCard) => {
      setKaart3(URL + newCard.card_image);
      setKaartValue3(newCard.card_value);
    });

    socket.on('nieuweKaart4', (newCard) => {
      setKaart4(URL + newCard.card_image);
      setKaartValue4(newCard.card_value);
    });

    //Laat alleen de hand zien als er een spel actief is.
    socket.on('handVisible', () =>{
      setHandVisible(!handVisible)
    })

    socket.on('handNotVisible', () =>{
      setHandVisible(handVisible)
    })
 
  }, [ENDPOINT, location.search]);
 

  //stuurt de kaart data naar de server als een speler een kaart speelt
  const placeCard1 = () => {
    console.log('clicked card 1');
    socket.emit('placeACard', { id: idState, position: 1, cardValue: kaartValue1, cardImage: kaart1 });
  }
  const placeCard2 = () => {
    console.log('clicked card 2');
    socket.emit('placeACard', { id: idState, position: 2, cardValue: kaartValue2, cardImage: kaart2  });
  }
  const placeCard3 = () => {
    console.log('clicked card 3');
    socket.emit('placeACard', { id: idState, position: 3, cardValue: kaartValue3, cardImage: kaart3  });
  }
  const placeCard4 = () => {
    console.log('clicked card 4');
    socket.emit('placeACard', { id: idState, position: 4, cardValue: kaartValue4, cardImage: kaart4  });
  }
  

    return (
      <section className = {handVisible ? 'hand': 'hand--notvisible'}>
        <div className ='singleCard'>
          <img onClick={placeCard1} className = 'singleCard__img' src={kaart1}/>
        </div>
        <div className ='singleCard'>
          <img onClick={placeCard2} className = 'singleCard__img' src={kaart2}/>
        </div>
        <div className ='singleCard'>
          <img onClick={placeCard3} className = 'singleCard__img' src={kaart3}/>
        </div>
        <div className ='singleCard'>
          <img onClick={placeCard4} className = 'singleCard__img' src={kaart4}/>
        </div>
      </section>
    );
}

export default SpelerHand;
