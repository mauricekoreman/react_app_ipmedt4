import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import queryString from 'query-string';
import {socket} from '../../Socket'

import TextContainer from '../../components/TextContainer/TextContainer';
import './SubLobby.css';


const Demarreren = (props) => {
  const [name, setName] = useState('');
  const [room, setRoom] = useState('');
  const [users, setUsers] = useState('');
  const ENDPOINT = 'localhost:5000';
  const location = useLocation();

  useEffect(() => {
    const { name, room, subroom } = queryString.parse(location.search);

    setRoom(room);


    socket.emit('join', { name, room }, (error) => {
      if(error) {
        alert(error);
      }
    });

  }, [ENDPOINT, location.search]);

  useEffect(() => {

    socket.on("roomData", ({ users }) => {
      setUsers(users);
    });
  }, []);

  return (
    <section className="sublobby">
      <article className="sublobby__delen">
        <label>Deelbare link: </label>
        <a className="sublobby__delen__link" href="#">http://localhost:3000/join?room={room}</a>  
      </article>
      <TextContainer users={users}/>
    </section>

  );
}

export default Demarreren;