import React from 'react';

// import person icon
import {ReactComponent as Person} from '../../icons/person-24px.svg';

import './TextContainer.css';

const TextContainer = ({ users }) => (
  <article className="section__textContainer">
    {
      users
        ? (
          <div className="activeContainer">
            {users.map(({name}) => (
              <div key={name} className="player-active">
                <Person className='player-active__icon' />
                <p className='player-active__name'>{name}</p>
              </div>
            ))}
          </div>
        )
        : null
    }
  </article>
);

export default TextContainer;
