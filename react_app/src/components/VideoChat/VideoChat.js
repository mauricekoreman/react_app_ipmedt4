import React, {useState, useEffect} from 'react';
import './VideoChat.css';
import queryString from 'query-string';

import { useLocation } from "react-router-dom";

let roomNaam;

const VideoChat = () => {
  
  const ENDPOINT = 'localhost:5000';
  const location = useLocation();
  const [room, setRoom] = useState('');
  const [videoLink, setvideoLink] = useState('https://meet.jit.si/' + room);

  useEffect(() => {
    const {room} = queryString.parse(location.search);

    //Een link aanmaken voor jitsi zodat mensen in dezelfde room in dezelfde videocall komen
    roomNaam = room;
    setvideoLink('https://meet.jit.si/Ditiseenroommetnaam' + roomNaam )

  }, [ENDPOINT, location.search]);

  return (
    <section className = 'videoChat'>
      <div>{videoLink}</div>
      <iframe allow="microphone; camera" className = 'videoFrame' src={videoLink} title="VideoIframe"></iframe>
   </section>
  );
}

export default VideoChat;
