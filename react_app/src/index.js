// 1) React importeren
import React from "react";
// 2) React DOM importeren zodat we dingen in de DOM kunnen stoppen
import ReactDOM from "react-dom";
// 3) App importeren zodat we de 'app' pagina kunnen laten zien
import App from "./App";
// 4) App component in de DOM stoppen
ReactDOM.render(
    <App />,


    document.getElementById("root")
);
