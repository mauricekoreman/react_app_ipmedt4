import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import queryString from 'query-string';
import './Aanmelden.css';
import { useLocation } from "react-router-dom";

export default function Aanmelden() {
  const [name, setName] = useState('');
  const [room, setRoom] = useState('');
  const location = useLocation();
  
  const ENDPOINT = 'localhost:5000';
  useEffect(() => {
    const { room } = queryString.parse(location.search);
    setRoom(room);
    
  }, [ENDPOINT, location.search]);
  
  return (
    <section className="section1">
      <form onSubmit="" className="section1__form">
        <label className="section1__form__label">Start een nieuwe sessie
          <input placeholder="Naam" className="section1__form__inputNaam" type="text" onChange={(event) => setName(event.target.value)}/>
        </label>
        <Link onClick={e => (!name || !room) ? e.preventDefault() : null} to={`/Just Chatting?name=${name}&room=${room}Just Chatting`}>
          <button className='section1__form__link__button' type="submit">Sign In</button>
        </Link>
      </form>
    </section>
  );
}

