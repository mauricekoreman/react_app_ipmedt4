import React from "react";

import './Demarreren.css'

import SubLobby from '../../components/SubLobby/SubLobby';
import VideoChat from '../../components/VideoChat/VideoChat';
import GameDashboard from "../../components/GameDashboard/GameDashboard";
import Game from '../../components/Game/Game';

const Demarreren = (props) => {

  const cardClicked = url => {
    props.history.push('/' + url);
  }
  return (
   
      <div className='demarreren__content'>
        <GameDashboard cardClicked={cardClicked}/>
        <Game />
        <VideoChat />
      </div>
    
  );
}

export default Demarreren;
