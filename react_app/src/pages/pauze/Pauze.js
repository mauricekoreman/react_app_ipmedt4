import React, { useState, useEffect } from "react";
import './Pauze.css';

import VideoChat from '../../components/VideoChat/VideoChat';
import GameDashboard from "../../components/GameDashboard/GameDashboard";

const Pauze = (props) => {
  const cardClicked = url => {
    props.history.push('/' + url);
  }

  return (
    <div className='pauze__content'>
      <GameDashboard cardClicked={cardClicked}/>
      <VideoChat />
    </div>
  );
}

export default Pauze;
